import { Component, Input, forwardRef } from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';


@Component({
  selector: 'centavos',
  templateUrl: './centavos.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => Centavos),
      multi: true
    }
  ]
})
export class Centavos implements ControlValueAccessor   {
  @Input() valorMoeda: number;
  quantidade = 0;
  btnsDisabled: boolean;

  aumentar() {
    this.quantidade += 1;
  }

  subtrair() {
    this.quantidade -= 1;
  }

  get btnMenos() {
    if (this.quantidade > 0) {
      return false;
    }
    return true;
  }

  writeValue(value: number) {
    this.quantidade = value;
  }

  registerOnChange() {

  }

  registerOnTouched() {

  }

  setDisabledState(isDisabled: boolean) {
    this.btnsDisabled = isDisabled;
  }

}
