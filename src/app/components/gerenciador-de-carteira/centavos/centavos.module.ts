import { Centavos } from './centavos';
import { NgModule } from '@angular/core';

import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [Centavos],
  imports: [ButtonModule],
  exports: [Centavos]
})
export class CentavosModule {

}
