import { CentavosModule } from './centavos/centavos.module';
import { NgModule } from '@angular/core';
import { GerenciadorDeCarteira } from './gerenciador-de-carteira.component';


@NgModule({
  imports: [CentavosModule],
  declarations: [GerenciadorDeCarteira],
  exports: [GerenciadorDeCarteira]
})
export class GerenciadorDeCarteiraModule {

}
