import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  templateUrl: './gerenciador-de-carteira.component.html'
})
export class GerenciadorDeCarteira implements OnInit {

  carteiraForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.carteiraForm = this.fb.group({
      cincoCentavos: {value: null, disabled: true}
    });


  }

  control() {
    return this.carteiraForm.controls.cincoCentavos.value;
  }



}
