import { GerenciadorDeCarteiraModule } from './../components/gerenciador-de-carteira/gerenciador-de-carteira.module';
import { GerenciadorDeCarteira } from './../components/gerenciador-de-carteira/gerenciador-de-carteira.component';
import { HomeContainer } from './home-container/home-container.component';
import { HomeRouting } from './home-routing.module';
import { PageNotFoundComponent } from './../page-not-found/page-not-found.component';
import { PanelModule } from 'primeng/panel';
import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import {PanelMenuModule} from 'primeng/panelmenu';

@NgModule({
  declarations: [
    HomeComponent,
    HomeContainer,
    PageNotFoundComponent
  ],
  imports: [
    HomeRouting,
    PanelModule,
    PanelMenuModule,
    GerenciadorDeCarteiraModule
  ],
  exports: [
    HomeComponent,
    HomeContainer,
    PageNotFoundComponent
  ]
})
export class HomeModule {

}
