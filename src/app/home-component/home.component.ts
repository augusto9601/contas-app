import { MenuModel } from './../model/menu-model';
import { MenuItem } from 'primeng/components/common/menuitem';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  items: Array<MenuItem>;

  ngOnInit() {
    this.items = [
      {
        label: 'Gerenciador de Carteira',
        icon: 'pi pi-pw pi-money-bill',
        items: [
          {
            label: 'Gerenciar',
            icon: 'pi pi-pw pi-user',
            routerLink: 'gerenciador'
          },
          {
            label: 'Despesa',
            icon: 'pi pi-pw pi-plus'
          }
        ]
      }
    ];
  }


}
