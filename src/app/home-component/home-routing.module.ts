import { GerenciadorDeCarteira } from './../components/gerenciador-de-carteira/gerenciador-de-carteira.component';
import { PageNotFoundComponent } from './../page-not-found/page-not-found.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeContainer } from './home-container/home-container.component';

const routes: Routes = [
  { path: 'home', component: HomeContainer },
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'gerenciador', component: GerenciadorDeCarteira },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class HomeRouting {}
