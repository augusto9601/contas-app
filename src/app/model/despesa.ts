import { Conta } from './conta';

export class Despesa {
  id: number;
  descricao: string;
  valor: number;
  contaOrigem: Conta;
}
