import { MenuItem } from 'primeng/components/common/menuitem';

export class MenuModel {

  arrayItems = new Array<MenuItem>();

  public static instance() {
    return new MenuModel();
  }

  public pai(label?: string, icon?: string) {
    this.arrayItems.push({label: label, icon: icon });
    return this;
  }

  arrayMenuItems(): Array<MenuItem> {
    return this.arrayItems;
  }



}
