import { Conta } from './conta';

export class ContaCarteira extends Conta {
  id: number;
  cincoCentavos: number;
  dezCentavos: number;
  vinteECincoCentavos: number;
  cinquentaCentavos: number;
  umReal: number;
  doisReais: number;
  cincoReais: number;
  dezReais: number;
  vinteReais: number;
  cinquentaReais: number;
  cemReais: number;

}
